######
README
######

Reusable graphical content, preferably as SVG.

Categories
##########

Logos
=====

Logos of brands or softwares, probably copyrighted.

Illustrations
=============

Graphics, diagrams, etc.

Notes and resources
###################

Icons
=====

* https://simpleicons.org/
* https://github.com/edent/SuperTinyIcons
* https://useiconic.com/open
* https://github.com/EmojiTwo/emojitwo (https://github.com/joypixels/emojione)
* https://github.com/twitter/twemoji

As fonts
========

* https://www.nerdfonts.com/ / https://github.com/ryanoasis/nerd-fonts

Responsive SVG
==============

* https://tympanus.net/Tutorials/ResponsiveSVGs/
* logos/suse/portus.svg

SVG as sprites
==============

* https://css-tricks.com/svg-fragment-identifiers-work/
* https://www.broken-links.com/2012/08/14/better-svg-sprites-with-fragment-identifiers/
* https://longsonr.wordpress.com/2012/08/13/svg-fragment-identifiers/
* https://www.w3.org/TR/SVG/linking.html#LinksIntoSVG
* logos/airbus/swf.svg
